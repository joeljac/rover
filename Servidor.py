# -*- coding: utf-8 -*-

import socket
import time
import serial

# Socket
Connection = None
IP = 'localhost'
SocketPort = 9999
MaxConnections = 5

# Arduino
PuertoUSB = '/dev/ttyACM0'
Arduino = None

def main():
	print "Iniciando servidor"
	crearSocket()

def crearSocket():
	print "creando socket"
	serversocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	print "esperando conexión"
	serversocket.bind((IP, SocketPort))
	serversocket.listen(MaxConnections) # Espera la conexion
	Connection, address = serversocket.accept()
	buf = Connection.recv(64)  # el primer mensaje del cliente es Conexion Exitosa
	print buf
	# Iniciando conexión serial
	Arduino = serial.Serial(PuertoUSB, 9600, timeout=1)
	# Retardo para establecer la conexión serial
	time.sleep(1.8)
	print "Esperando Ordenes"
	while True:
		buf = Connection.recv(64)
		# envia el dato al arduino
		Arduino.write(buf)
		# imprime la respusta del arduino
		getSerialValue = Arduino.readline()
		Connection.send(getSerialValue)
		


main()
