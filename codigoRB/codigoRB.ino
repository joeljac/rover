/**      Rover
*  Motor Rover     (MR)
*  Direccion Rover (DR)
*/

#define MR1 22
#define DR1 23
#define MR2 24
#define DR2 25
#define MR3 26
#define DR3 27
#define MR4 28
#define DR4 29

//  Brazo

#define MBR 30 // motor que rota la base
#define DBR 31 // direccion de la base
#define MBB 32 // motor del brazo
#define DBB 33 // direccion del brazo
#define MBA 34 // motor antebrazo
#define DBA 35 // direccion antebrazo
#define MBM 36 // motor muñeca
#define DBM 37 // Direccion muñeca

//Ultrasonicos

//#define SDT1 2
//#define SDE1 3
//
//#define SDT2 4
//#define SDE2 5
//
////variable de control
char val;
//long durationA, distanceA, durationB, distanceB;

void setup ()
{
// Rover
 pinMode(MR1, OUTPUT);
 pinMode(DR1, OUTPUT);
 pinMode(MR2, OUTPUT);
 pinMode(DR2, OUTPUT);
 pinMode(MR3, OUTPUT);
 pinMode(DR3, OUTPUT);
 pinMode(MR4, OUTPUT);
 pinMode(DR4, OUTPUT);

// Brazo
 pinMode(MBR, OUTPUT);
 pinMode(DBR, OUTPUT);
 pinMode(MBB, OUTPUT);
 pinMode(DBB, OUTPUT);
 pinMode(MBA, OUTPUT);
 pinMode(DBA, OUTPUT);
 pinMode(MBM, OUTPUT);
 pinMode(DBM, OUTPUT);

////Ultrasonicos
// pinMode(SDT1, OUTPUT);
// pinMode(SDE1, INPUT);
// pinMode(SDT2, OUTPUT);
// pinMode(SDE2, INPUT);

 Serial.begin(9600);
 MR(0); // Apaga el Rover
 MBOff(); // Apaga el Brazo

}// end of setup


void loop (){
 if(Serial.available()) {
    val = Serial.read();
    if(val == 'a' || val == 'r' || val == 's' || val == 'i' || val == 'd'){
      Rover (val);
    }else if(val == '1' || val == '2' || val == '3' || val == '4' || val == '5' || val == '6' || val == '7' || val == '8')  {
      Brazo (val);
    }else if(val == 'K'){ //los sensores detiene tanto al rover como al brazo
      Serial.println("Apagado de Emergencia");
      MBOff();
      MR(0);
    }else if(val == 'z'){
      Serial.println("Modo Autonomo Iniciado");
//      Autonomo();
    }

 }
}// end of loop

void Brazo(char val){
  switch(val){

  // case 1 y 2 rota la base

  case '1':
   Serial.println("Rotando Base");
   digitalWrite (DBR, 1);   // la base rota hacia ?
   analogWrite  (MBR, 200);
  break;

  case '2':
   Serial.println("Rotando Base");
   digitalWrite (DBR, 0);   // la base rota hacia?
   analogWrite  (MBR, 200);
  break;

  // case 3 y 4 controla el brazo
  case '3':
   Serial.println("Brazo Activado");
   digitalWrite (DBB, 1);   // el brazo sube/baja?
   analogWrite  (MBB, 200);
  break;

  case '4':
    Serial.println("Brazo Activado");
    digitalWrite (DBB, 0);   // el brazo sube/baja?
    analogWrite  (MBB, 200);
  break;

  // case 5 y 6 controla el antebrazo
  case '5':
    Serial.println("Antebrazo Activo");
    digitalWrite (DBA,1);   // el antebrazo sube/baja?
    analogWrite  (MBA,200);
  break;

  case '6':
    Serial.println("Antebrazo Activo");
    digitalWrite (DBA,0);   // el antebrazo sube/baja?
    analogWrite  (MBA,200);
  break;

  // case 7 y 8 controla la muñeca
  case '7':
    Serial.println("Muñeca Activada");
    digitalWrite (DBM,1);   //muñeca sube/baja?
    analogWrite  (MBM,200);
  break;

  case '8':
    Serial.println("Muñeca Activada");
    digitalWrite (DBM,0);   //muñeca sube/baja?
    analogWrite  (MBM,200);
  break;
  }// end switch
}// end Brazo

void MBOff(){
 analogWrite(MBR, 0); // motor que rota la base
 analogWrite(MBB, 0); // motor del brazo
 analogWrite(MBA, 0); // motor antebrazo
 analogWrite(MBM, 0); // motor muñeca
}

void Rover(char val){
 switch(val){
   case 'a':
     Serial.println("adelante");
     DR(0,1,0,1);
     MR(200);
   break;

   case 'd':
      Serial.println("derecha");
      DR(0,1,1,0);
      MR(200);
   break;

   case 'r':
      Serial.println("reversa");
      DR(1,0,1,0);
      MR(200);
   break;

   case 'i':
      Serial.println("izquierda");
      DR(1,0,0,1);
      MR(200);
   break;

   case 's':
      Serial.println("detener");
      MR(0);
   break;

   }// end switch
}

void MR(int velocidad){
 analogWrite (MR1, velocidad);
 analogWrite (MR2, velocidad);
 analogWrite (MR3, velocidad);
 analogWrite (MR4, velocidad);
}


void DR(int r1, int r2, int r3, int r4){
 digitalWrite (DR1, r1);
 digitalWrite (DR2, r2);
 digitalWrite (DR3, r3);
 digitalWrite (DR4, r4);
}

//
//void Distancia(){
//
//  digitalWrite(SDT1, LOW);
//  delayMicroseconds(2);
//  digitalWrite(SDT1, HIGH);
//  delayMicroseconds(10);
//  digitalWrite(SDT1, LOW);
//
//  durationA = pulseIn(SDE1, HIGH);
//  distanceA = (durationA/2) / 29.1;
//
//  delayMicroseconds(20);
//
//  digitalWrite(SDT2, LOW);
//  delayMicroseconds(2);
//  digitalWrite(SDT2, HIGH);
//  delayMicroseconds(10);
//  digitalWrite(SDT2, LOW);
//
//  durationB = pulseIn(SDE2, HIGH);
//  distanceB = (durationB/2) / 29.1;
//
//}
//
//
//
//void Autonomo(){
//
//boolean next = true;
//
//do{
//
//  Distancia();
//
//  // si puede continuar, avanzara.
//  if(distanceA > 15 && distanceB > 15){
//    //continuar
//    DR(0,1,0,1);
//    MR(200);
//  }
//
//  while(distanceA <= 15 || distanceB <= 15) {
//     MR(0);
//     //giro izquierda
//     DR(1,0,0,1);
//     MR(200);
//     delay(300);
//     Distancia();
//   }
//
//   if(Serial.available()){
//      next = false;
//      break;
//   }
//
// }while(next);
//}
